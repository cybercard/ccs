var app = require('express')();
var server = require('http').Server(app);
//var io = require('socket.io')(server);

var ccm = require("ccm");
var ueshka = require('ueshka');
var user = require('user');
var news = require('news');
var sc = require('socket-controller');
var chat = require('chat');

server.listen(7645);

sc.listen(server);

app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, OPTIONS, PUTCH');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

app.get('/', function (req, res) {
    var path = req.query.path;

    if(path == 'ccm') {
        ccm.run(req.query, function(err, data) {
            res.send({err: err, data: data});
        });
    } else {
        ueshka.get(path, req.query, function(err, data) {
            res.send({data: data});
        });
    }
});